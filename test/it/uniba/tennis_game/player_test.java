package it.uniba.tennis_game;

import static org.junit.Assert.*;

import org.junit.Test;

import it.uniba.tennisgame.Player;

public class player_test {

	@Test
	public void scoreShouldBeIncreased() {
		//Arrange
		Player player = new Player("Federer", 0); //
		
		//Act
		player.incrementScore();
		
		//Assert
		assertEquals(1, player.getScore());
	}
	
	@Test
	public void scoreShouldNotBeIncreased() {
		//Arrange
		Player player = new Player("Federer", 0); //
	
		
		//Assert
		assertEquals(0, player.getScore());
	}
	
	@Test
	public void scoreShouldBelove() {
		//Arrange
		Player player = new Player("Federer", 0);
		
		//Act
		
		String scoreAsString = player.getScoreAsString();
		//Assert
		
		assertEquals("love", scoreAsString);
		
	}
	

	@Test
	public void scoreShouldBeFifteen() {
		//Arrange
		Player player = new Player("Federer", 1);
		
		//Act
		
		String scoreAsString = player.getScoreAsString();
		//Assert
		
		assertEquals("fifteen", scoreAsString);
		
	}
	

	@Test
	public void scoreShouldBeThirty() {
		//Arrange
		Player player = new Player("Federer", 2);
		
		//Act
		
		String scoreAsString = player.getScoreAsString();
		//Assert
		
		assertEquals("thirty", scoreAsString);
		
	}
	

	@Test
	public void scoreShouldBeFourty() {
		//Arrange
		Player player = new Player("Federer", 3);
		
		//Act
		
		String scoreAsString = player.getScoreAsString();
		//Assert
		
		assertEquals("forty", scoreAsString);
		
	}
	

	@Test
	public void scoreShouldBeNull() {
		//Arrange
		Player player = new Player("Federer", -1);
		
		//Act
		
		String scoreAsString = player.getScoreAsString();
		//Assert
		
		assertNull(scoreAsString);
		
	}
	
	@Test
	public void scoreShouldBeNullPositive() {
		//Arrange
		Player player = new Player("Federer", 4);
		
		//Act
		
		String scoreAsString = player.getScoreAsString();
		//Assert
		
		assertNull(scoreAsString);
		
	}
	
	@Test
	public void shouldBeTie() {
		//Arrange
		Player player1 = new Player("Federer", 2);
		Player player2 = new Player("Nadal", 2);
		//Act
		boolean tie = player1.isTieWith(player2);
		
		//assert
		assertTrue(tie);
		
	}
	
	@Test
	public void shouldNotBeTie() {
		//Arrange
		Player player1 = new Player("Federer", 2);
		Player player2 = new Player("Nadal", 3);
		//Act
		boolean tie = player1.isTieWith(player2);
		
		//assert
		assertFalse(tie);
		
	}
	
	@Test
	public void shouldHaveAtleastFortyPoints() {
		//Arrange
			Player player1 = new Player("Federer", 3);
		//Act
			boolean outcome = player1.hasAtLeastFortyPoints();
		//Assert
			assertTrue(outcome);
	}
	
	@Test
	public void shouldNotHaveAtleastFortyPoints() {
		//Arrange
			Player player1 = new Player("Federer", 2);
		//Act
			boolean outcome = player1.hasAtLeastFortyPoints();
		//Assert
			assertFalse(outcome);
	}
	
	@Test
	public void shouldHaveLessThanFortyPoints() {
	//Arrange
		Player player1 = new Player("Federer", 2);
	//Act
		boolean outcome = player1.hasLessThanFortyPoints();
	//Assert
		assertTrue(outcome);
	}
	
	@Test
	public void shouldNotHaveLessThanFortyPoints() {
	//Arrange
		Player player1 = new Player("Federer", 4);
	//Act
		boolean outcome = player1.hasLessThanFortyPoints();
	//Assert
		assertFalse(outcome);
	}
	
	@Test
	public void shouldHaveMoreThanFourtyPoints() {
	//Arrange
		Player player1 = new Player("Federer", 4);
	//Act
		boolean outcome = player1.hasMoreThanFourtyPoints();
	//Assert
		assertTrue(outcome);
	}
	
	@Test
	public void shouldNotHaveMoreThanFourtyPoints() {
	//Arrange
		Player player1 = new Player("Federer", 2);
	//Act
		boolean outcome = player1.hasMoreThanFourtyPoints();
	//Assert
		assertFalse(outcome);
	}
	
	@Test
	public void shouldHaveOnePointAdvantageOn() {
		//Arrange
			Player player1 = new Player("Federer", 3);
			Player player2 = new Player("Nadal", 2);
		//Act
			boolean outcome = player1.hasOnePointAdvantageOn(player2);
				
		//assert
			assertTrue(outcome);
	}
	
	@Test
	public void shouldNotHaveOnePointAdvantageOn() {
		//Arrange
			Player player1 = new Player("Federer", 2);
			Player player2 = new Player("Nadal", 3);
		//Act
			boolean outcome = player1.hasOnePointAdvantageOn(player2);
				
		//assert
			assertFalse(outcome);
	}
	
	@Test
	public void shouldHaveTwoPointAdvantageOn() {
		//Arrange
			Player player1 = new Player("Federer", 3);
			Player player2 = new Player("Nadal", 1);
		//Act
			boolean outcome = player1.hasAtLeastTwoPointsAdvantageOn(player2);
				
		//assert
			assertTrue(outcome);
	}
	
	@Test
	public void shouldNotHaveTwoPointAdvantageOn() {
		//Arrange
			Player player1 = new Player("Federer", 1);
			Player player2 = new Player("Nadal", 3);
		//Act
			boolean outcome = player1.hasAtLeastTwoPointsAdvantageOn(player2);
				
		//assert
			assertFalse(outcome);
	}
	

}
