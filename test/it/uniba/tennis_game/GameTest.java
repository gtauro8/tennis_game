package it.uniba.tennis_game;

import static org.junit.Assert.*;

import org.junit.Test;

import it.uniba.tennisgame.Game;

public class GameTest {

	@Test
	public void testFifteenThirty() throws Exception {
		
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String p1 = game.getPlayerName1();
		String p2 = game.getPlayerName2();
		
		//Act
		
		game.incrementPlayerScore(p1);
		
		game.incrementPlayerScore(p2);
		game.incrementPlayerScore(p2);
		String status = game.getGameStatus();
		
		//Assert
		assertEquals("Federer fifteen - Nadal thirty", status);
	}
	
	@Test
	public void testFortyThirty() throws Exception{
		//Arrange
				Game game = new Game("Federer", "Nadal");
				String p1 = game.getPlayerName1();
				String p2 = game.getPlayerName2();
				
				//Act
				
				game.incrementPlayerScore(p1);
				game.incrementPlayerScore(p1);
				game.incrementPlayerScore(p1);
				
				game.incrementPlayerScore(p2);
				game.incrementPlayerScore(p2);
				String status = game.getGameStatus();
				
				//Assert
				assertEquals("Federer forty - Nadal thirty", status);
				
	}
	
	@Test
	public void testPlayerWins() throws Exception{
		//Arrange
				Game game = new Game("Federer", "Nadal");
				String p1 = game.getPlayerName1();
				String p2 = game.getPlayerName2();
				
				//Act
				
				game.incrementPlayerScore(p1);
				game.incrementPlayerScore(p1);
				
				
				game.incrementPlayerScore(p2);
				game.incrementPlayerScore(p2);
				
				game.incrementPlayerScore(p1);
				game.incrementPlayerScore(p1);
				String status = game.getGameStatus();
				
				//Assert
				assertEquals("Federer wins", status);
				
	}
	
	@Test
	public void testFifteenForty() throws Exception{
		//Arrange
				Game game = new Game("Federer", "Nadal");
				String p1 = game.getPlayerName1();
				String p2 = game.getPlayerName2();
				
				//Act
				
				game.incrementPlayerScore(p1);
			
				game.incrementPlayerScore(p2);
				game.incrementPlayerScore(p2);
				game.incrementPlayerScore(p2);
				
				String status = game.getGameStatus();
				
				//Assert
				assertEquals("Federer fifteen - Nadal forty", status);
				
	}
	
	@Test
	public void testDeuce() throws Exception{
		//Arrange
				Game game = new Game("Federer", "Nadal");
				String p1 = game.getPlayerName1();
				String p2 = game.getPlayerName2();
				
				//Act
				
				game.incrementPlayerScore(p1);
				game.incrementPlayerScore(p1);
				game.incrementPlayerScore(p1);


			
				game.incrementPlayerScore(p2);
				game.incrementPlayerScore(p2);
				game.incrementPlayerScore(p2);
				String status = game.getGameStatus();
				
				//Assert
				assertEquals("Deuce", status);
				
	}
	
	@Test
	public void testAdvantagePlayer() throws Exception{
		//Arrange
				Game game = new Game("Federer", "Nadal");
				String p1 = game.getPlayerName1();
				String p2 = game.getPlayerName2();
				
				//Act
				
				game.incrementPlayerScore(p1);
				game.incrementPlayerScore(p1);
				game.incrementPlayerScore(p1);


			
				game.incrementPlayerScore(p2);
				game.incrementPlayerScore(p2);
				game.incrementPlayerScore(p2);
				
				game.incrementPlayerScore(p1);

				
				String status = game.getGameStatus();
				
				//Assert
				assertEquals("Advantage Federer", status);
				
	}
	
	@Test
	public void testNadalWins() throws Exception{
		//Arrange
				Game game = new Game("Federer", "Nadal");
				String p1 = game.getPlayerName1();
				String p2 = game.getPlayerName2();
				
				//Act
				
				game.incrementPlayerScore(p1);
				

				game.incrementPlayerScore(p2);
				game.incrementPlayerScore(p2);
				game.incrementPlayerScore(p2);
				game.incrementPlayerScore(p2);

				
				String status = game.getGameStatus();
				
				//Assert
				assertEquals("Nadal wins", status);
				
	}
	
	@Test
	public void testAdvantagePlayer2() throws Exception{
		//Arrange
				Game game = new Game("Federer", "Nadal");
				String p1 = game.getPlayerName1();
				String p2 = game.getPlayerName2();
				
				//Act
				
				game.incrementPlayerScore(p1);
				game.incrementPlayerScore(p1);
				game.incrementPlayerScore(p1);


			
				game.incrementPlayerScore(p2);
				game.incrementPlayerScore(p2);
				game.incrementPlayerScore(p2);
				game.incrementPlayerScore(p2);

				
				String status = game.getGameStatus();
				
				//Assert
				assertEquals("Advantage Nadal", status);
				
	}
	

	@Test
	public void testDeuceAfterAdvantage() throws Exception{
		//Arrange
				Game game = new Game("Federer", "Nadal");
				String p1 = game.getPlayerName1();
				String p2 = game.getPlayerName2();
				
				//Act
				
				game.incrementPlayerScore(p1);
				game.incrementPlayerScore(p1);
				game.incrementPlayerScore(p1);


			
				game.incrementPlayerScore(p2);
				game.incrementPlayerScore(p2);
				game.incrementPlayerScore(p2);
				
				game.incrementPlayerScore(p1);
				game.incrementPlayerScore(p2);

				
				String status = game.getGameStatus();
				
				//Assert
				assertEquals("Deuce", status);
				
	}
	
	

}


